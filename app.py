from flask import Flask, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import mysql.connector
from forms import EquipoForm


app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"]= 'mysql+mysqlconnector://masynjha_jhava:j8H&a4v4@localhost/testmvc'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "masiste"

db = SQLAlchemy(app)

class Equipo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(40))
    pais = db.Column(db.String(20))

    def __repr__(self):
        return"<Equipo %r>" % self.nombre

with app.app_context():                                         
    db.create_all()
    db.session.commit()

@app.route('/')
def inicio():
    return render_template('index.html')

@app.route('/nuevo_equipo', methods=['GET', 'POST'])
def nuevo_equipo():
    form = EquipoForm() 
    if form.validate_on_submit():
        nombre = form.nombre.data
        pais = form.pais.data

        nuevo_equipo = Equipo(nombre=nombre, pais=pais)

        with app.app_context():
            db.session.add(nuevo_equipo)
            db.session.commit()

        return 'Equipo registrado exitosamente'
    
    return render_template('nuevo_equipo.html', form=form)

if __name__ == "__main__":
    app.run(debug = True)