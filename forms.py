from flask_wtf import FlaskForm
from wtforms import StringField

class EquipoForm(FlaskForm):
    nombre = StringField('Nombre del equipo')
    pais = StringField('Pais del equipo')
